<?php
class PHPNonce {

	const SALT = 'this-is-just-a-default-salt-so-replace-it-with-some-salt';
	const EXPIRATION = 43200;
	const LENGTH = 10;
  	const CHECK_DEFINED = true;
  	const DEFINE = 'SomeDefine';
	
	/**
	 *	Creates a nonce (number used once)
	 *
	 *	Runs a hash algorithm on a name, session data and timestamp to generate
	 *	a one-time-numer. Saved as a session value
	 *	
	 *	@param string $name the nonce name
	 *	@returns string the nonce
	 */
	public static function create( $name )  {
		
		if ( self::CHECK_DEFINED && !defined( self::DEFINE ) )
			die( 'go away' );
		
		if ( empty( $_SESSION[ 'nonces' ] ) )	
			$_SESSION[ 'nonces' ] = array();
		
		$timestamp = time();
		
		$_SESSION[ 'nonces' ][ $name ] = array( 
			'token' => PHPNonce::_create( $name, $timestamp ),
			'timestamp' => $timestamp
		);
		
		return $_SESSION[ 'nonces' ][ $name ][ 'token' ];
	}
	
	/**
	 *	Actually creates the nonce
	 *
	 *	@params string $name the name of the nonce
	 * 	@params int $timestamp the current UNIX timestamp
	 *	@returns string the nonce value
	 */
	protected static function _create( $name, $timestamp ) {
		return substr( md5( $name . self::SALT . session_id() . $timestamp ), 0, self::LENGTH );
	}

	/**
	 *	Verifies a nonce (number used once)
	 *
	 *	Simply reruns the algorithm to create the nonce and tries to match
	 *	it with the stored value. If it doesn't exist or doesn't match, returns
	 *	false. If it is expired, returns false. If it is valid, returns true;
	 *	
	 *	@param string $token the received nonce value
	 *	@param string $name the nonce name
	 *	@returns boolean the valid state of the token
	 */
	public static function verify( $token, $name )  { 

		if ( empty( $_SESSION[ 'nonces' ] ) )
			return false;
		if ( empty( $_SESSION[ 'nonces' ][ $name ] ) )
			return false;
		
		$token = $_SESSION[ 'nonces' ][ $name ][ 'token' ];
		$timestamp = $_SESSION[ 'nonces' ][ $name ][ 'timestamp' ];
		
		if ( $timestamp + self::EXPIRATION < time() ) {
			PHPNonce::remove( $name );
			return false;
		}
		
		return $token == self::_create( $name, $timestamp );
	}
	
	/**
	 *	Removes the nonce
	 */
	public static function remove( $name ) {
		
		if ( empty( $_SESSION[ 'nonces' ] ) )	
			return false;
		if ( empty( $_SESSION[ 'nonces' ][ $name ] ) )
			return false;
		
		unset( $_SESSION[ 'nonces' ][ $name ] );
		return true;
	}
}
?>
