php-nonces
==========

Adds number-used-once functionality to your PHP program. 

## Functions
````PHP
$nonce = PHPNonce::create( 'action-name' );
````
The `create( $name )` creates a nonce with the `$name` and returns the value. 
The nonce remains valid as long as the `session` is valid and the `EXPIRATION` time has not passed. 
Returns a string of lenght `LENGTH` and saves it in the `$_SESSION`

````PHP
$nonce = $_POST[ 'security' ];
$valid = PHPNonce::verify( $nonce, 'action-name' );
````
The `verify( $nonce, $name )` checks if the given `$nonce` was started in `$name`.
Returns true if the value is exactly the same as stored and the `EXPIRATION` time has not passed.
Don't forget to `remove` the nonce after verification. (You might want to verify it twice, so it's
not automatically disposed off).

````PHP
PHPNonce::remove( 'action-name' );
````
The `remove( $name )` removes the nonce with the name `$name`.

## Constants
* `SALT`: a string that is used as nonce salt. Should be secret and only available on the server.
* `EXPIRATION`: an integer that indicates the number of seconds a nonce is valid.
* `LENGTH`: an integer that indicates the number of characters in the output.
* `CHECK_DEFINED`: a boolean that restricts creation of nonces only when `DEFINE` is defined.
* `DEFINE`: a string that holds the definition that should be defined when creating nonces.

## Example
````PHP
<?php
//==========================================
// Nonce creation and defines
//==========================================
define( 'SomeDefine', '1.2' );
$nonce = PHPNonce::create( 'my-action' );
?>

<form action="#" method="post">
    <input type="hidden" name="security" value="<?php echo $nonce; ?>">
    <input type="text" name="myaction[field]">
    <input type="submit" name="myaction[submit]">
</form>

<?php
//==========================================
// Form input validation and processing
//==========================================
if ( !empty( $_POST[ 'myaction' ] ) ) :
    if ( PHPNonce::verify( $_POST[ 'security' ], 'my-action' ) ) :
        // Save the data
    else :
        // Display some message?
    endif;
    PHPNonce::remove( 'my-action' );
endif;
?>
````
